/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shape;

/**
 *
 * @author jaiminlakhani
 */
public class ShapesSimulation {
    public static void main(String[] args) {
    Shape s1 = new Circle(5.5);
    Shape s2 = new Square(4.8);
    
    System.out.println("The area of a circle is: " + s1.getArea()); 
    System.out.println("The perimeter of a circle is: " + s1.getPerimeter());
    System.out.println("The area of a square is: " + s2.getArea());
    System.out.println("The perimeter of a square is: " + s2.getPerimeter());
    }
}
