/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package shape;

/**
 *
 * @author jaiminlakhani
 */
public abstract class Shape {
    public abstract double getArea();
            
    public abstract double getPerimeter();
}
