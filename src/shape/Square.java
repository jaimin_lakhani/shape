/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shape;

/**
 *
 * @author jaiminlakhani
 */
public class Square extends Shape{
   double side;
   
   Square(double side) {
       this.side = side;
   }
   
   @Override public double getArea() {
       return side * side;
   }
   
   @Override public double getPerimeter() {
       return 4 * side;
   }
}
